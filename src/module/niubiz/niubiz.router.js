import express from "express";
import { getSessionToken } from "./niubiz.service.js";

const routerNiubiz = express.Router();

routerNiubiz.get("/", (req, res) => {
  res.send("Hola Mundo con Nibuiz!");
});

routerNiubiz.post("/", getSessionToken);

export default routerNiubiz;
