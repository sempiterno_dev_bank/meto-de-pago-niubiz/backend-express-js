import express from "express";
import router from "./module/index.js";
import routerNiubiz from "./module/niubiz/niubiz.router.js";
import cors from "cors";

const app = express();
const PORT = 3001;

app.use(
  cors({
    origin: "*", // Permitir solo solicitudes de este dominio
    methods: ["GET", "POST"], // Permitir solo estos métodos
    allowedHeaders: ["Content-Type", "Authorization"], // Permitir solo estos encabezados
    credentials: true, // Permitir cookies de origen cruzado
  })
);

// Tus rut

app.use("/", router);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/niubiz", routerNiubiz);

app.listen(PORT, () => {
  console.log(`Servidor corriendo en http://localhost:${PORT}`);
});
